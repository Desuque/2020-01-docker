#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./test.sh <PORT>"
	exit 1
fi

if [ -x "$(command -v wget)" ] ; then
	REQUEST="wget -qO-"
elif [ -x "$(command -v curl)" ] ; then
	REQUEST="curl -s"
else
	echo "Please install wget or curl to make http requests"
	exit 3
fi

TEST_PORT=$1
RESULT=$($REQUEST localhost:$TEST_PORT/data)

if [ "$RESULT" = "It works" ] ; then
	echo "OK"
else
	echo "Received: '$RESULT'"
	echo "Failed"
	exit 2
fi
